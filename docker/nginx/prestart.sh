# generate custom ssl certificate
echo "Generate nginx.key, nginx.crt!"
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/ssl/nginx.key -out /etc/nginx/ssl/nginx.crt -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.nginx.com"
sleep 1