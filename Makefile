stop:
		@echo "[+] Stop All Containers"
		docker compose down && docker stop $(docker ps -q) || true;

start: stop
		@echo "[+] Start All Containers"
		docker compose up -d --build

build:
		docker compose -f build.yml build
		@echo "[+] push to dockerhub"
		docker compose -f build.yml push

sync:
		@echo "[+] Sync Certificates and Nginx Configs"
		docker compose exec control_agent ./control-agent.py sync --reconfigure

vacuum:
		@echo "[+] Clean logs, journalctl, apt"
		sudo apt-get autoclean
		sudo journalctl --vacuum-time=3d
		sudo truncate -s 0 /var/lib/docker/containers/*/*-json.log || true;
		sudo truncate -s 0 /var/log/**/*.log || true;

wipe: stop vacuum
	@echo "[+] stop running container, delete all stopped containers, data, logs"
	cd /srv/ && docker system prune --all --force --volumes;

upgrade:
	@echo "[+] Upgrading control-agent"
	git pull && docker compose up -d --build