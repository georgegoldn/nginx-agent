#!/usr/bin/env bash
if [[ $1 == "" ]]; then
  echo "Set settings for Control-Agent!"
  exit 0
fi
decrypted=$(echo "$1" | base64 --decode)
IFS=';' read control_agent_settings SOCKD_USER_NAME SOCKD_USER_PASSWORD <<<$decrypted
echo $control_agent_settings
printf "PROXY_LOGIN=$SOCKD_USER_NAME\nPROXY_PASSWORD=$SOCKD_USER_PASSWORD\n" > /srv/control-agent/.env
jq -n --arg appname "$appname" "$control_agent_settings" > /srv/data/settings.json

cd /srv/control-agent/ && make start && cd /srv
